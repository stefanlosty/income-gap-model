# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 15:18:11 2020

@author: SurfacePro3
"""
import random as rand
from statistics import *
from math import *
import income_vs_time_curves as ivtc
import numpy as np
from matplotlib.pyplot import *

# global variables
population_size = 20
initial_min_income = 0
initial_max_income = 10000
timesteps = 200
window_size = 20

def main(curve,curve_pars):
    global income_matrix, income_minimums, income_maximums, population
    income_matrix,income_minimums,income_maximums = create_income_matrix(population_size,initial_min_income,initial_max_income,timesteps,curve,curve_pars)
    population,peasants,workers,professionals,bourgeoisie,boundaries_matrix = assign_classes(income_matrix,income_minimums,income_maximums)
    
    global rel_dep_matrix, peasants_rel_dep_matrix, workers_rel_dep_matrix, professionals_rel_dep_matrix, bourgeoisie_rel_dep_matrix
    peasants_rel_dep_matrix = relative_deprivation(peasants,[boundaries_matrix[0],boundaries_matrix[1]])
    workers_rel_dep_matrix = relative_deprivation(workers,[boundaries_matrix[1],boundaries_matrix[2]])
    professionals_rel_dep_matrix = relative_deprivation(professionals,[boundaries_matrix[2],boundaries_matrix[3]])
    bourgeoisie_rel_dep_matrix = relative_deprivation(bourgeoisie,[boundaries_matrix[3],boundaries_matrix[4]])
    rel_dep_matrix = np.concatenate([peasants_rel_dep_matrix,workers_rel_dep_matrix,professionals_rel_dep_matrix,bourgeoisie_rel_dep_matrix])
    
    global moving_average_matrix, lost_income_ratio_matrix
    moving_average_matrix = moving_average(income_matrix)
    lost_income_ratio_matrix = lost_income_ratio(income_matrix,moving_average_matrix)
    
    global mean_lost_income_ratio
    revolutionary_sentiment_matrix = revolutionary_sentiment(population)
    mean_rev_sentiment = get_mean(revolutionary_sentiment_matrix)
    mean_lost_income_ratio = get_mean(lost_income_ratio_matrix)
    
    return rel_dep_matrix, mean_lost_income_ratio, mean_rev_sentiment

class peasant:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment
        
class worker:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment

class professional:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment
        
class bourgeois:
    def __init__(self,income,rel_dep,lost_income_ratio,rev_sentiment):
        self.income = income
        self.rel_dep = rel_dep
        self.lost_income_ratio = lost_income_ratio
        self.rev_sentiment = rev_sentiment
       
def create_income_matrix(population_size,min_income,max_income,timesteps,curve,pars):
    initial_distribution = []
    for i in range(population_size):
        initial_distribution.append(rand.randrange(min_income,max_income))
    initial_distribution.sort()
    income_matrix,income_minimums,income_maximums = curve(initial_distribution,min_income,max_income,timesteps,pars)     
    return income_matrix,income_minimums,income_maximums

def assign_classes(income_matrix,income_minimums,income_maximums):
    a = int(population_size/4)
    b = int(population_size*2/4)
    c = int(population_size*3/4)
    
    peasants = []
    workers = []
    professionals = []
    bourgeoisie = []
    population = []
    for i in range(a):
        peasants.append(peasant(income_matrix[:,i],[],[],[]))
        population.append(peasants[i])
    for i in range(a,b):
        workers.append(worker(income_matrix[:,i],[],[],[]))
        population.append(workers[i-a])
    for i in range(b,c):
        professionals.append(professional(income_matrix[:,i],[],[],[]))
        population.append(professionals[i-b])
    for i in range(c,population_size):
        bourgeoisie.append(bourgeois(income_matrix[:,i],[],[],[]))
        population.append(professionals[i-c])
        
    boundaries_0 = income_minimums
    boundaries_1 = []; boundaries_2 = []; boundaries_3 = []
    for i in range(timesteps):
        boundaries_1.append(mean([income_matrix[i,a-1],income_matrix[i,a]]))
        boundaries_2.append(mean([income_matrix[i,b-1],income_matrix[i,b]]))
        boundaries_3.append(mean([income_matrix[i,c-1],income_matrix[i,c]]))
    boundaries_4 = income_maximums
    boundaries_matrix = np.array([boundaries_0,boundaries_1,boundaries_2,boundaries_3,boundaries_4])
    
    return population,peasants,workers,professionals,bourgeoisie,boundaries_matrix

def relative_deprivation(class_list,boundaries_pair):
    class_income_matrix = []
    for person in class_list:
        class_income_matrix.append(person.income)
    class_income_matrix = np.array(class_income_matrix)
    
    class_widths = []
    class_income_maximums = []
    for i in range(timesteps):
        class_widths.append(boundaries_pair[1][i]-boundaries_pair[0][i])
        class_income_maximums.append(max(class_income_matrix[:,i]))
    
    for i in range(timesteps):    
        for person in class_list:
            d = class_income_maximums[i]-person.income[i]
            D = boundaries_pair[1][i]-person.income[i]
            person.rel_dep.append(sqrt(d*D)/class_widths[i])
            
    class_rel_dep_matrix = []
    for person in class_list:
        class_rel_dep_matrix.append(person.rel_dep)    
    return class_rel_dep_matrix

def moving_average(income_matrix):
    moving_average_matrix = []
    for person in range(population_size):
        i = 0
        moving_average = []
        while i <= len(income_matrix[:,person])-window_size:
            window = income_matrix[i:i+window_size,person]
            window_average = sum(window)/window_size
            moving_average.append(window_average)
            i+=1
        moving_average_matrix.append(moving_average)
    moving_average_matrix = np.transpose(moving_average_matrix)
    return moving_average_matrix

def lost_income_ratio(income_matrix,moving_average_matrix):
    lost_income_ratio_matrix = []
    for person in range(population_size):
        instant_lost_income = []
        for i in range(len(moving_average_matrix[:,person])):
            instant_lost_income.append((moving_average_matrix[i,person]-income_matrix[i+window_size-1,person])/moving_average_matrix[i,person])
            
        cumulative_lost_income = np.zeros(len(moving_average_matrix[:,person]))
        lost_income_ratio = []
        for i in range(len(moving_average_matrix[:,person])):
            trigger = instant_lost_income[i-1]*instant_lost_income[i]
            if trigger > 0:
                cumulative_lost_income[i] = instant_lost_income[i]+cumulative_lost_income[i-1]
            elif trigger <= 0:
                cumulative_lost_income[i] = instant_lost_income[i]
            lost_income_ratio.append(cumulative_lost_income[i])
        lost_income_ratio_matrix.append(lost_income_ratio)
        population[person].lost_income_ratio = lost_income_ratio
    lost_income_ratio_matrix = np.transpose(lost_income_ratio_matrix)
    return lost_income_ratio_matrix

def revolutionary_sentiment(population):
    revolutionary_sentiment_matrix = np.empty([timesteps-window_size+1,population_size])
    for p in range(population_size):
        for i in range(timesteps-window_size+1):
            population[p].rev_sentiment.append(mean([population[p].rel_dep[i+window_size-1],population[p].lost_income_ratio[i]]))
            revolutionary_sentiment_matrix[i,p] = population[p].rev_sentiment[i]
    return revolutionary_sentiment_matrix

def get_mean(matrix):
    mean_array = []
    for i in range(timesteps-window_size+1):
        mean_array.append(mean(matrix[i]))
    return mean_array

"""
Plot code is currently not well generalised.
"""
def save_plot(y,t,x_label,y_label,my_legend,title,filename):
    if my_legend == ["Individual peasant"]:
        for i in range(5):
            plot(t,y[i],'g')
        legend(my_legend)
    elif my_legend == ["Income","Moving Average"]:
        for i in range(len(y)):
            plot(t[i],y[i],label = my_legend[i])
            legend()
    elif my_legend == ["Constant","Exponential","Sinusoidal"]:
        for i in range(len(y)):
            plot(t,y[i],label = my_legend[i])
            legend()
    figtext(0.19,0.9,title, fontsize = 14, weight = 'bold')
    xlabel(x_label, fontsize=14)
    ylabel(y_label, fontsize=14)
    savefig(filename, dpi=400)

constant_d_m,constant_m_l_i_r,constant_m_r_s = main(ivtc.constant,-0.005)  
sin_d_m,sin_m_l_i_r,sin_m_r_s = main(ivtc.sin_curve,[8*pi,1])   
exp_d_m,exp_m_l_i_r,exp_m_r_s = main(ivtc.exponential_curve,0.99)

t = np.linspace(0,199,200)
t_shift = np.linspace(19,199,181)
#save_plot([constant_m_l_i_r,exp_m_l_i_r,sin_m_l_i_r],t_shift,"Timesteps","Mean Population RLI",["Constant","Exponential","Sinusoidal"],"Mean Population RLI for Income-Time Curves","MLIR.png")
#save_plot([income_matrix[:,4],moving_average_matrix[:,4]],[t,t_shift],"Timesteps","Value",["Income","Moving Average"],"Actual Income and Expected Income","ma.png")
save_plot(exp_d_m,t,"Timesteps","Relative Deprivation",["Individual peasant"],"Relative Deprivation for Peasants","RD.png")    

