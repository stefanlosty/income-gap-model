# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 00:02:48 2020

@author: SurfacePro3
"""
import numpy as np

def constant(initial_distribution,initial_min,initial_max,timesteps,coefficient):
    income_minimums = []
    income_maximums = []
    P = len(initial_distribution)
    constant_matrix = np.zeros((timesteps,P))
    for i in range(timesteps):
        income_minimums.append(initial_min*(1+coefficient*i))
        income_maximums.append(initial_max*(1+coefficient*i))
        for j in range(P):
            constant_matrix[i,j] = initial_distribution[j]*(1+coefficient*i)        
    return constant_matrix,income_minimums,income_maximums

def sin_curve(initial_distribution,initial_min,initial_max,timesteps,pars):
    domain,coefficient = pars
    income_minimums = []
    income_maximums = []
    P = len(initial_distribution)
    sin_matrix = np.zeros((timesteps,P))
    for i in range(timesteps):
        income_minimums.append(initial_min*(1+coefficient*np.sin(i*domain/timesteps)))
        income_maximums.append(initial_max*(1+coefficient*np.sin(i*domain/timesteps)))
        for j in range(P):
            sin_matrix[i,j] = initial_distribution[j]*(1+coefficient*np.sin(i*domain/timesteps))  
    return sin_matrix,income_minimums,income_maximums

def exponential_curve(initial_distribution,initial_min,initial_max,timesteps,exponent):
    income_minimums = []
    income_maximums = []
    P = len(initial_distribution)
    exponential_matrix = np.zeros((timesteps,P))
    for i in range(timesteps):
        income_minimums.append(initial_min**(exponent**i))
        income_maximums.append(initial_max**(exponent**i))
        for j in range(P):
            exponential_matrix[i,j] = initial_distribution[j]**(exponent**i)      
    return exponential_matrix,income_minimums,income_maximums