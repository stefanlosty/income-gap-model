# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 13:23:28 2020

@author: SurfacePro3
"""
import random
import numpy as np

def main(n):
    individual_income = create_population(n)
    average_income = np.mean(individual_income)
    rebel_array = rebel_probability(n, individual_income, average_income)
    rebels_count = np.sum(rebel_array)
    return rebels_count

def create_population(n):
    income_array = np.zeros(n)
    for i in range(n):
        income_array[i] = random.randrange(1000)
    return income_array
    
def rebel_probability(n, income_array, average_income):
    rebel_array = np.zeros(n)
    for i in range(n-1):
        rho = income_array[i]/average_income
        if rho < 1:
            rebel_array[i] = 1 - rho
        else:
            rebel_array[i] = 0
    return rebel_array

rebels = main(1000)